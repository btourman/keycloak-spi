package fr.btourman;

import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.storage.user.UserRegistrationProvider;

import java.util.logging.Logger;

public class UserRegistration implements UserRegistrationProvider {

    private Logger logger = Logger.getLogger(UserRegistration.class.getName());

    @Override
    public UserModel addUser(RealmModel realmModel, String s) {
        logger.warning("Add user");
        logger.warning(s);
        return null;
    }

    @Override
    public boolean removeUser(RealmModel realmModel, UserModel userModel) {
        return false;
    }
}
