package fr.btourman;

import org.keycloak.Config;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

import java.util.logging.Logger;

public class MyEventListenerProviderFactory implements EventListenerProviderFactory {

    private Logger logger = Logger.getLogger(MyEventListenerProviderFactory.class.getName());

    public void init(Config.Scope config) {
    }

    @Override
    public EventListenerProvider create(KeycloakSession keycloakSession) {
        return new MyEventListenerProvider(keycloakSession);
    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {
    }

    @Override
    public void close() {

    }

    @Override
    public String getId() {
        return "user-registration-listener";
    }
}
