package fr.btourman;

import com.github.shyiko.dotenv.DotEnv;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class MyEventListenerProvider implements EventListenerProvider {

    private KeycloakSession session;
    private Logger logger = Logger.getLogger(MyEventListenerProvider.class.getName());

    public MyEventListenerProvider(KeycloakSession session) {
        this.session = session;
    }

    @Override
    public void onEvent(Event event) {
        if (event.getType() == EventType.REGISTER) {
            RealmModel realm = session.realms().getRealm(event.getRealmId());
            UserModel user = session.users().getUserById(event.getUserId(), realm);
            this.sendRabbitmq(user);
        }
    }

    private void sendRabbitmq(UserModel user) {
        try {
            Map<String, String> dotEnv = DotEnv.load();
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(dotEnv.getOrDefault("RABBITMQ_HOST", "localhost"));
            factory.setPort(Integer.parseInt(dotEnv.getOrDefault("RABBITMQ_PORT", "5672")));
            factory.setVirtualHost(dotEnv.getOrDefault("RABBITMQ_VHOST", "/"));
            factory.setUsername(dotEnv.getOrDefault("RABBITMQ_USERNAME", ConnectionFactory.DEFAULT_USER));
            factory.setPassword(dotEnv.getOrDefault("RABBITMQ_PASSWORD", ConnectionFactory.DEFAULT_PASS));
            factory.setRequestedHeartbeat(0);

            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            String routingKey = "client.register";
            String exchangeName = "client-exchange";
            String queueName = "client-service";
            String message = "{\"id\":\"" + user.getId() +
                    "\",\"firstname\":\"\"" +
                    ",\"lastname\":\"\"" +
                    ",\"plan\":\"" + user.getFirstAttribute("plan") + "\"" +
                    ",\"phone\":\"" + user.getFirstAttribute("phone") + "\"" +
                    ",\"street\":\"" + user.getFirstAttribute("street") + "\"" +
                    ",\"city\":\"" + user.getFirstAttribute("locality") + "\"" +
                    ",\"postcode\":\"" + user.getFirstAttribute("postal_code") + "\"" +
                    ",\"country\":\"" + user.getFirstAttribute("country") + "\"" +
                    ",\"email\":\"" + user.getEmail() + "\"}";

            channel.exchangeDeclare(exchangeName, "topic", true);
            channel.queueDeclare(queueName, true, false, false, null);
            channel.queueBind(queueName, exchangeName, routingKey);

            channel.basicPublish(exchangeName, routingKey, null, message.getBytes());
            connection.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    private void sendInsightly(UserModel user) {
//        String url = "https://api.insight.ly/v2.2/Leads";
//
//        HttpClient client = HttpClients.createDefault();
//        HttpPost post = new HttpPost(url);
//
//        ObjectMapper mapper = new ObjectMapper();
//        ObjectNode root = mapper.createArrayNode();
//        root.put("LAST_NAME",user.getLastName());
//        root.put("FIRST_NAME",user.getFirstName());
//        root.put("EMAIL_ADDRESS",user.getEmail());
//        root.put("LEAD_STATUS_ID",2148836);
//
//        StringEntity requestEntity = new StringEntity(
//                JSON_STRING,
//                ContentType.APPLICATION_JSON);
//        post.setEntity(requestEntity);
//        try {
//            client.execute(post);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        var options = {
//                url: 'https://api.insight.ly/v2.2/Leads',
//                headers: {
//            "Content-Type": 'application/json',
//                    Authorization: 'Basic ZTc0NGZkMDUtOGMyNC00NGNhLTkzZmEtNzk2MjE5YmJjMzI5Og=='
//        },
//        body: JSON.stringify({
//                FIRST_NAME: body.firstname,
//                LAST_NAME: body.lastname,
//                EMAIL_ADDRESS: body.email,
//                LEAD_STATUS_ID: 2148836,
//                LEAD_SOURCE_ID: body.source,
//                PHONE_NUMBER: body.phone,
//                TITLE: body.title,
//                ADDRESS_STREET: body.address.street,
//                ADDRESS_CITY: body.address.city,
//                ADDRESS_POSTCODE: body.address.postcode,
//                ADDRESS_COUNTRY: body.address.country,
//            })
//        }
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {
    }

    @Override
    public void close() {

    }
}
